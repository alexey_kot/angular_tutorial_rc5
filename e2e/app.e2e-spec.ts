import { AngularRc5QuickstartPage } from './app.po';

describe('angular-rc5-quickstart App', function() {
  let page: AngularRc5QuickstartPage;

  beforeEach(() => {
    page = new AngularRc5QuickstartPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
